package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyFirstController {

    //
    @Autowired
    private MyFirstService myFirstService;

    //Im root / d.h localhost:8081 ruft das auf
    @GetMapping
    public HelloDto hello(@RequestParam(required = false) String user) {
        return myFirstService.hello(user);
        //Im Service ist die Logik enthalten
        //Controller nur für HTTP-Request zuständig
    }
    //RequestParam kann man ?user=Max in URL angeben um query param zu übergeben
    //node = req.query

}
